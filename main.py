import discord, json, os
from discord import app_commands


if os.path.exists(os.getcwd() + "/config.json"):
    with open("./config.json")as f:
        configData = json.load(f)

else:
    configTemplate = {
    "Token": "<Bot Token Here>",
    "GuildID": "<GuildID Here>"
    }
    print('Config file not found, creating one for you...')
    with open(os.getcwd() + "/config.json", "w+") as f:
        json.dump(configTemplate, f)
        exit()

token = str(configData["Token"])
guildID = discord.Object(configData["GuildID"])

class MyClient(discord.Client):
    def __init__(self, *, intents: discord.Intents):
        super().__init__(intents=intents)
        self.tree = app_commands.CommandTree(self)

    async def setup_hook(self):
        self.tree.copy_global_to(guild=guildID)
        await self.tree.sync(guild=guildID)

intents = discord.Intents.default()
client = MyClient(intents=intents)

@client.event
async def on_ready():
    print(f'Logged in as {client.user} (ID: {client.user.id})')
    print('------')

@client.tree.command(name='vending_machines')
async def vending(interaction: discord.Interaction):
    embed = discord.Embed(
        title='How to use vending machines!',
        description='open the crafting menu while next to the vending machines `b` by default\nclick on the tab `money` and all the vending machines buyables will show there.',
    )
    embed.set_image(url='https://cdn.discordapp.com/attachments/859710530884337684/1067114451721003008/image.png')
    await interaction.response.send_message(embed=embed)

client.run(token)